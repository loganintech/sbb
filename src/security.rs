pub mod pgp;
pub mod cleartext;


pub trait EncryptionMethod {
    fn encrypt(plaintext: &str) -> String;
    fn decrypt(ciphertext: &str) -> String;
}