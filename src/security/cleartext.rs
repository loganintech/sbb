pub struct Cleartext;

impl super::EncryptionMethod for Cleartext {
    fn encrypt(plaintext: &str) -> String {
        plaintext.into()
    }

    fn decrypt(ciphertext: &str) -> String {
        ciphertext.into()
    }
}
