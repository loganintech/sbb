use std::io::{self, Write};

use sequoia_openpgp::{
    constants::DataFormat,
    packet::key::SecretKey,
    packet::{PKESK, SKESK},
    parse::stream::*,
    serialize::stream::*,
    KeyID, Result, TPK,
};

pub struct PGP;

impl super::EncryptionMethod for PGP {
    fn encrypt(plaintext: &str) -> String {
        unimplemented!();
    }
    fn decrypt(ciphertext: &str) -> String {
        unimplemented!();
    }
}

fn encrypt(data_dest: &mut Write, plaintext: &str, receivers: &[&TPK]) -> Result<()> {
    let message = Message::new(data_dest);

    let encryptor = Encryptor::new(message, &[], receivers, EncryptionMode::ForTransport)?;

    let mut writer = LiteralWriter::new(encryptor, DataFormat::Unicode, None, None)?;
    writer.write_all(plaintext.as_bytes())?;
    writer.finalize()?;

    Ok(())
}

fn decrypt(data_dest: &mut Write, ciphertext: &[u8], receipient: &TPK) -> Result<()> {
    // Make a helper that that feeds the recipient's secret key to the
    // decryptor.
    let helper = Helper { secret: receipient };

    // Now, create a decryptor with a helper using the given TPKs.
    let mut decryptor = Decryptor::from_bytes(ciphertext, helper)?;

    // Decrypt the data.
    io::copy(&mut decryptor, data_dest)?;

    Ok(())
}

struct Helper<'a> {
    secret: &'a TPK,
}

impl<'a> VerificationHelper for Helper<'a> {
    fn get_public_keys(&mut self, _ids: &[KeyID]) -> Result<Vec<TPK>> {
        // Return public keys for signature verification here.
        Ok(Vec::new())
    }

    fn check(&mut self, _sigs: Vec<Vec<VerificationResult>>) -> Result<()> {
        // Implement your signature verification policy here.
        Ok(())
    }
}

impl<'a> DecryptionHelper for Helper<'a> {
    fn get_secret(
        &mut self,
        _pkesks: &[&PKESK],
        _skesks: &[&SKESK],
    ) -> Result<Option<Secret>> {
        // The encryption key is the first and only subkey.
        let key = self
            .secret
            .subkeys()
            .nth(0)
            .map(|binding| binding.subkey().clone())
            .unwrap();

        // The secret key is not encrypted.
        let secret = if let Some(SecretKey::Unencrypted { ref mpis }) = key.secret() {
            mpis.clone()
        } else {
            unreachable!()
        };

        Ok(Some(Secret::Asymmetric {
            identity: self.secret.fingerprint(),
            key: key,
            secret: secret,
        }))
    }
}
