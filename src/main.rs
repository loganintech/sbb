mod security;

type Result<T> = std::result::Result<T, Box<std::error::Error>>;

use clap::{Arg, App};

fn main() -> Result<()> {
    let matches = App::new("SBB - Saso Bulitin Board")
        .version("1.0")
        .author("Logan Saso <logan.saso2016@gmail.com>")
        .about("Sends messages to the Saso bulitin board.")
        .arg(
            Arg::with_name("encryption_method")
                .required(true)
                .short("e")
                .help("Sets the encryption method."),
        )
        .get_matches();

    let encryption_method = match matches.value_of("encryption_method").unwrap().to_lowercase().as_ref() {
        "p" | "pgp" => /*Use PGP encryption*/,
        "c" | "cleartext" => /*Use Cleartext*/,
        _ => /*Use PGP encryption*/
    };

    Ok(())
}
